﻿using System;
using System.Linq;
using System.IO;


namespace FileFix
{
    class Program
    {
        static void Main(string[] args)
        {
            bool err;
            string[] files = Directory.GetFiles("."); // TESTING: Directory.GetFiles("./temp/");

            if (args.Length == 0)
            {
                Console.WriteLine("No switches! \r\n\r\n");

                Console.WriteLine("File Fix 1.0\n\r");
                Console.WriteLine("File Fix will add a prefix or suffix to all the files in the directory run.");
                Console.WriteLine("switches: \r\n p ---- Set prefix \r\n s ---- Set suffix.  \r\nSimple enough... ");
                Console.WriteLine("Usage: \r\nfilefix p _pre \n\r");
                Environment.Exit(0);
            }
            if (args[0] != "p" && args[0] != "s")
            {
                Console.WriteLine("Invalid switch... '{0}' --  {1}", args[0], args[1]);
                Environment.Exit(0);
            }


            if (args[0].Length > 30)
            {
                Console.WriteLine("Prefix or Suffix Must be no more than 30 characters.");
                Environment.Exit(0);
            }

            Console.WriteLine("File Fix 1.0\n\r");
            Console.WriteLine("File Fix will add a prefix or suffix to all the files in the directory run.");
            Console.WriteLine("switches: \r\n p ---- Set prefix \r\n s ---- Set suffix.  \r\nSimple enough... ");
            Console.WriteLine("Usage: \r\nfilefix p _pre \n\r");

            foreach (string fileName in files)
            {
                err = FileProcessor(fileName, args[0], args[1]);
            }

            Environment.Exit(0);
        }

        public static bool FileProcessor(string filename, string placement, string str)
        {
            string newfilename = "NA";

            
             string _filename = filename;    // original filename 
             filename = filename.Replace(@".\", "");

            try
            {
                if (placement.ToLower() == "p")
                {
                    newfilename = str + filename;
                    Console.WriteLine("Adding prefix to {0} -> {1}", filename, newfilename);
                }
                else if (placement.ToLower() == "s")
                {
                    string[] filesep = filename.Split('.');
                    if (filesep.Count() > 2)
                    {
                        Console.WriteLine("ERROR: {0} has more than 1 dot... (We'll fix this in a future version. Sorry.)", filename);
                        return false;
                    }
                    filesep[0] += str;
                    newfilename = filesep[0] + "." + filesep[1];
                    Console.WriteLine("Adding Suffix to {0} -> {1}", filename, newfilename);
                }
                if (newfilename == "NA")
                    return false;

                // TESTING: File.Move(_filename, "./temp/" + newfilename);
                File.Move(filename, newfilename);

                return true;
            }
            catch (Exception e)
            {
                Console.WriteLine(e.ToString());
                Environment.Exit(0);
                return false;
            }
        }
    }
}