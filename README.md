## File Prefix/Suffix util ##

Simple asp.net console project that will add a prefix or suffix to all files in a directory.  
Did this mainly as an exercise for myself

# NOTE: #

To make this a real system-wide Utility

+ Make a folder in your c drive (or whatever dir you want, I used c:\util\)
+ Set the PATH Environment Variable in Windows: On the command line type: set PATH=%PATH%;c:\util\filefix.exe

**Clearly, do this at your own discretion.**  

Feel free to add stuff or change around things if you think it'll be better.